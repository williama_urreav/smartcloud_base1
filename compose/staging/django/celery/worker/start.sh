#!/bin/sh

set -o errexit
set -o pipefail
set -o nounset


celery -A control.taskapp worker -l INFO
