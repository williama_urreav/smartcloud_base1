# -*- coding: utf-8 -*-

from manage.crm.models import DeliveryNote
from manage.crm.factories.factory_sales_document import StandardSalesDocumentFactory


class StandardDeliveryNoteFactory(StandardSalesDocumentFactory):
    class Meta:
        model = DeliveryNote

    tracking_reference = "This is a tracking reference"
    status = "S"

