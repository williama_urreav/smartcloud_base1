# -*- coding: utf-8 -*-

import factory
from manage.crm.models import UnitTransform
from manage.crm.factories.factory_unit import StandardUnitFactory, SmallUnitFactory
from manage.crm.factories.factory_product_type import StandardProductTypeFactory


class StandardUnitTransformFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = UnitTransform
        django_get_or_create = ('from_unit',
                                'to_unit')

    from_unit = factory.SubFactory(StandardUnitFactory)
    to_unit = factory.SubFactory(SmallUnitFactory)
    product_type = factory.SubFactory(StandardProductTypeFactory)
    factor = 1.10
