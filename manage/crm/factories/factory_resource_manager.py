# -*- coding: utf-8 -*-

import factory
from manage.crm.models import ResourceManager
from manage.djangoUserExtension.factories.factory_user_extension import StandardUserExtensionFactory


class StandardResourceManagerFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ResourceManager

    user = factory.SubFactory(StandardUserExtensionFactory)
