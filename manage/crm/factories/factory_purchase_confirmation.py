# -*- coding: utf-8 -*-

from manage.crm.models import PurchaseConfirmation
from manage.crm.factories.factory_sales_document import StandardSalesDocumentFactory


class StandardPurchaseConfirmationFactory(StandardSalesDocumentFactory):
    class Meta:
        model = PurchaseConfirmation


