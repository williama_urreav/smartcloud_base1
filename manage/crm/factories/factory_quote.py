# -*- coding: utf-8 -*-

from manage.crm.models import Quote
from manage.crm.factories.factory_sales_document import StandardSalesDocumentFactory


class StandardQuoteFactory(StandardSalesDocumentFactory):
    class Meta:
        model = Quote

    valid_until = "2018-05-20"
    status = "C"
