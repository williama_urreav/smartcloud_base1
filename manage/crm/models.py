# -*- coding: utf-8 -*-

from manage.crm.contact.contact import *
from manage.crm.contact.customer_group import *
from manage.crm.contact.customer import *
from manage.crm.contact.postal_address import *
from manage.crm.contact.customer_billing_cycle import *
from manage.crm.contact.email_address import *
from manage.crm.contact.phone_address import *
from manage.crm.contact.supplier import *

from manage.crm.documents.contract import Contract, PostalAddressForContract
from manage.crm.documents.contract import PhoneAddressForContract, EmailAddressForContract
from manage.crm.documents.sales_document_position import Position, SalesDocumentPosition
from manage.crm.documents.sales_document import SalesDocument, PostalAddressForSalesDocument
from manage.crm.documents.sales_document import EmailAddressForSalesDocument, PhoneAddressForSalesDocument
from manage.crm.documents.sales_document import TextParagraphInSalesDocument
from manage.crm.documents.invoice import Invoice
from manage.crm.documents.purchase_confirmation import PurchaseConfirmation
from manage.crm.documents.purchase_order import PurchaseOrder
from manage.crm.documents.quote import Quote
from manage.crm.documents.payment_reminder import PaymentReminder
from manage.crm.documents.delivery_note import DeliveryNote

from manage.crm.product.currency import *
from manage.crm.product.price import *
from manage.crm.product.product import *
from manage.crm.product.product_type import *
from manage.crm.product.product_price import *
from manage.crm.product.customer_group_transform import *
from manage.crm.product.unit_transform import *
from manage.crm.product.tax import *
from manage.crm.product.unit import *

from manage.crm.reporting.agreement import *
from manage.crm.reporting.agreement_status import *
from manage.crm.reporting.agreement_type import *
from manage.crm.reporting.agreement_status import *
from manage.crm.reporting.estimation import *
from manage.crm.reporting.estimation_status import *
from manage.crm.reporting.human_resource import *
from manage.crm.reporting.resource_manager import *
from manage.crm.reporting.resource_type import *
from manage.crm.reporting.resource_price import *
from manage.crm.reporting.generic_task_link import *
from manage.crm.reporting.task import *
from manage.crm.reporting.task_link_type import *
from manage.crm.reporting.task_status import *
from manage.crm.reporting.work import *
from manage.crm.reporting.project import *
from manage.crm.reporting.project_link_type import *
from manage.crm.reporting.project_status import *
from manage.crm.reporting.generic_project_link import *
from manage.crm.reporting.reporting_period import *
from manage.crm.reporting.reporting_period_status import *






