# -*- coding: utf-8 -*-

from django.contrib import admin
from manage.crm.documents.quote import Quote, OptionQuote
from manage.crm.documents.purchase_confirmation import PurchaseConfirmation, OptionPurchaseConfirmation
from manage.crm.documents.delivery_note import DeliveryNote, OptionDeliveryNote
from manage.crm.documents.invoice import Invoice, OptionInvoice
from manage.crm.documents.payment_reminder import PaymentReminder, OptionPaymentReminder
from manage.crm.documents.purchase_order import PurchaseOrder, OptionPurchaseOrder
from manage.crm.documents.contract import Contract, OptionContract
from manage.crm.product.tax import Tax, OptionTax
from manage.crm.product.unit import Unit, OptionUnit
from manage.crm.product.product_type import ProductType, ProductTypeAdminView
from manage.crm.product.currency import Currency, OptionCurrency
from manage.crm.contact.customer import Customer, OptionCustomer
from manage.crm.contact.supplier import Supplier, OptionSupplier
from manage.crm.contact.customer_group import CustomerGroup, OptionCustomerGroup
from manage.crm.contact.customer_billing_cycle import CustomerBillingCycle, OptionCustomerBillingCycle
from manage.crm.contact.person import Person
from manage.crm.contact.contact import OptionPerson, CallForContact, VisitForContact
from manage.crm.contact.call import OptionCall, OptionVisit
from manage.crm.reporting.task import Task, TaskAdminView
from manage.crm.reporting.task_link_type import TaskLinkType, OptionTaskLinkType
from manage.crm.reporting.task_status import TaskStatus, OptionTaskStatus
from manage.crm.reporting.estimation_status import EstimationStatus, EstimationStatusAdminView
from manage.crm.reporting.agreement_status import AgreementStatus, AgreementStatusAdminView
from manage.crm.reporting.agreement_type import AgreementType, AgreementTypeAdminView
from manage.crm.reporting.resource_type import ResourceType, ResourceTypeAdminView
from manage.crm.reporting.human_resource import HumanResource, HumanResourceAdminView
from manage.crm.reporting.resource_manager import ResourceManager, ResourceManagerAdminView
from manage.crm.reporting.project import Project, ProjectAdminView
from manage.crm.reporting.project_link_type import ProjectLinkType, OptionProjectLinkType
from manage.crm.reporting.project_status import ProjectStatus, OptionProjectStatus
from manage.crm.reporting.work import Work, WorkAdminView
from manage.crm.reporting.reporting_period import ReportingPeriod, ReportingPeriodAdmin
from manage.crm.reporting.reporting_period_status import ReportingPeriodStatus, OptionReportingPeriodStatus


admin.site.register(Customer, OptionCustomer)
admin.site.register(CustomerGroup, OptionCustomerGroup)
admin.site.register(CustomerBillingCycle, OptionCustomerBillingCycle)
admin.site.register(Supplier, OptionSupplier)
admin.site.register(Person, OptionPerson)
admin.site.register(CallForContact, OptionCall)
admin.site.register(VisitForContact, OptionVisit)

admin.site.register(Contract, OptionContract)
admin.site.register(Quote, OptionQuote)
admin.site.register(PurchaseConfirmation, OptionPurchaseConfirmation)
admin.site.register(DeliveryNote, OptionDeliveryNote)
admin.site.register(Invoice, OptionInvoice)
admin.site.register(PaymentReminder, OptionPaymentReminder)
admin.site.register(PurchaseOrder, OptionPurchaseOrder)

admin.site.register(Unit, OptionUnit)
admin.site.register(Currency, OptionCurrency)
admin.site.register(Tax, OptionTax)
admin.site.register(ProductType, ProductTypeAdminView)

admin.site.register(Task, TaskAdminView)
admin.site.register(TaskLinkType, OptionTaskLinkType)
admin.site.register(TaskStatus, OptionTaskStatus)
admin.site.register(EstimationStatus, EstimationStatusAdminView)
admin.site.register(AgreementStatus, AgreementStatusAdminView)
admin.site.register(AgreementType, AgreementTypeAdminView)
admin.site.register(Work, WorkAdminView)
admin.site.register(HumanResource, HumanResourceAdminView)
admin.site.register(ResourceType, ResourceTypeAdminView)
admin.site.register(ResourceManager, ResourceManagerAdminView)
admin.site.register(Project, ProjectAdminView)
admin.site.register(ProjectLinkType, OptionProjectLinkType)
admin.site.register(ProjectStatus, OptionProjectStatus)
admin.site.register(ReportingPeriod, ReportingPeriodAdmin)
admin.site.register(ReportingPeriodStatus, OptionReportingPeriodStatus)
