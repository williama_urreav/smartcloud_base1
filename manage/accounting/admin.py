# -*- coding: utf-8 -*-

from django.contrib import admin

from manage.accounting.accounting.accounting_period import AccountingPeriod, OptionAccountingPeriod
from manage.accounting.accounting.booking import Booking, OptionBooking
from manage.accounting.accounting.account import Account, OptionAccount
from manage.accounting.accounting.product_category import ProductCategory, OptionProductCategory

admin.site.register(Account, OptionAccount)
admin.site.register(Booking, OptionBooking)
admin.site.register(ProductCategory, OptionProductCategory)
admin.site.register(AccountingPeriod, OptionAccountingPeriod)
