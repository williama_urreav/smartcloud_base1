from rest_framework import serializers

from manage.djangoUserExtension.user_extension.document_template import InvoiceTemplate
from manage.djangoUserExtension.user_extension.document_template import QuoteTemplate
from manage.djangoUserExtension.user_extension.document_template import DeliveryNoteTemplate
from manage.djangoUserExtension.user_extension.document_template import PaymentReminderTemplate
from manage.djangoUserExtension.user_extension.document_template import PurchaseOrderTemplate
from manage.djangoUserExtension.user_extension.document_template import PurchaseConfirmationTemplate
from manage.djangoUserExtension.user_extension.document_template import ProfitLossStatementTemplate
from manage.djangoUserExtension.user_extension.document_template import BalanceSheetTemplate
from manage.djangoUserExtension.user_extension.document_template import MonthlyProjectSummaryTemplate
from manage.djangoUserExtension.user_extension.document_template import WorkReportTemplate


class OptionDocumentTemplateJSONSerializer(serializers.HyperlinkedModelSerializer):
    title = serializers.CharField(required=False, read_only=True)
    xslFile = serializers.FileField(source='xsl_file', read_only=True)
    fopConfigFile = serializers.FileField(source='user', read_only=True)
    logo = serializers.FileField(read_only=True)


class OptionInvoiceTemplateJSONSerializer(OptionDocumentTemplateJSONSerializer):
    class Meta:
        model = InvoiceTemplate
        fields = ('title',
                  'xslFile',
                  'fopConfigFile',
                  'logo')


class OptionQuoteTemplateJSONSerializer(OptionDocumentTemplateJSONSerializer):
    class Meta:
        model = QuoteTemplate
        fields = ('title',
                  'xslFile',
                  'fopConfigFile',
                  'logo')


class OptionDeliveryNoteTemplateJSONSerializer(OptionDocumentTemplateJSONSerializer):
    class Meta:
        model = DeliveryNoteTemplate
        fields = ('title',
                  'xslFile',
                  'fopConfigFile',
                  'logo')


class OptionPaymentReminderTemplateJSONSerializer(OptionDocumentTemplateJSONSerializer):
    class Meta:
        model = PaymentReminderTemplate
        fields = ('title',
                  'xslFile',
                  'fopConfigFile',
                  'logo')


class OptionPurchaseOrderTemplateJSONSerializer(OptionDocumentTemplateJSONSerializer):
    class Meta:
        model = PurchaseOrderTemplate
        fields = ('title',
                  'xslFile',
                  'fopConfigFile',
                  'logo')


class OptionPurchaseConfirmationTemplateJSONSerializer(OptionDocumentTemplateJSONSerializer):
    class Meta:
        model = PurchaseConfirmationTemplate
        fields = ('title',
                  'xslFile',
                  'fopConfigFile',
                  'logo')


class OptionProfitLossStatementTemplateJSONSerializer(OptionDocumentTemplateJSONSerializer):
    class Meta:
        model = ProfitLossStatementTemplate
        fields = ('title',
                  'xslFile',
                  'fopConfigFile',
                  'logo')


class OptionBalanceSheetTemplateJSONSerializer(OptionDocumentTemplateJSONSerializer):
    class Meta:
        model = BalanceSheetTemplate
        fields = ('title',
                  'xslFile',
                  'fopConfigFile',
                  'logo')


class OptionMonthlyProjectSummaryTemplateTemplateJSONSerializer(OptionDocumentTemplateJSONSerializer):
    class Meta:
        model = MonthlyProjectSummaryTemplate
        fields = ('title',
                  'xslFile',
                  'fopConfigFile',
                  'logo')


class OptionWorkReportTemplateJSONSerializer(OptionDocumentTemplateJSONSerializer):
    class Meta:
        model = WorkReportTemplate
        fields = ('title',
                  'xslFile',
                  'fopConfigFile',
                  'logo')
