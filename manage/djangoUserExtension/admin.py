# -*- coding: utf-8 -*-


from manage.djangoUserExtension.models import *
from manage.djangoUserExtension.user_extension.user_extension import UserExtension, OptionUserExtension
from manage.djangoUserExtension.user_extension.template_set import TemplateSet, OptionTemplateSet
from manage.djangoUserExtension.user_extension.document_template import InvoiceTemplate
from manage.djangoUserExtension.user_extension.document_template import QuoteTemplate
from manage.djangoUserExtension.user_extension.document_template import DeliveryNoteTemplate
from manage.djangoUserExtension.user_extension.document_template import PaymentReminderTemplate
from manage.djangoUserExtension.user_extension.document_template import PurchaseOrderTemplate
from manage.djangoUserExtension.user_extension.document_template import ProfitLossStatementTemplate
from manage.djangoUserExtension.user_extension.document_template import BalanceSheetTemplate
from manage.djangoUserExtension.user_extension.document_template import OptionDocumentTemplate


admin.site.register(UserExtension, OptionUserExtension)
admin.site.register(TemplateSet, OptionTemplateSet)
admin.site.register(InvoiceTemplate, OptionDocumentTemplate)
admin.site.register(QuoteTemplate, OptionDocumentTemplate)
admin.site.register(DeliveryNoteTemplate, OptionDocumentTemplate)
admin.site.register(PaymentReminderTemplate, OptionDocumentTemplate)
admin.site.register(PurchaseOrderTemplate, OptionDocumentTemplate)
admin.site.register(PurchaseConfirmationTemplate, OptionDocumentTemplate)
admin.site.register(ProfitLossStatementTemplate, OptionDocumentTemplate)
admin.site.register(BalanceSheetTemplate, OptionDocumentTemplate)
admin.site.register(MonthlyProjectSummaryTemplate, OptionDocumentTemplate)
admin.site.register(WorkReportTemplate, OptionDocumentTemplate)
