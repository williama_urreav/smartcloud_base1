# PRECAUTION: avoid production dependencies that aren't in development

-r ./base.txt

gunicorn==20.0.4  # https://github.com/benoitc/gunicorn
psycopg2==2.8.4 --no-binary psycopg2  # https://github.com/psycopg/psycopg2
raven==6.10.0  # https://github.com/getsentry/raven-python

# Django
# ------------------------------------------------------------------------------
django-storages[boto3]==1.9.1  # https://github.com/jschneier/django-storages
django-anymail==7.0.0  # https://github.com/anymail/django-anymail
django-debug-toolbar==2.2  # https://github.com/jazzband/django-debug-toolbar
Faker==4.0.1


better_exceptions==0.2.2
pyupio==1.0.2
ipython==7.4.0
jupyter==1.0.0
pydotplus
